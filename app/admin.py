from django.contrib import admin
from .models import Post, Project

class MyModelAdmin(admin.ModelAdmin):
    list_filter = ('field', 'sub_field')

admin.site.register(Post)
admin.site.register(Project, MyModelAdmin)