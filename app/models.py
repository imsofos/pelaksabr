from django.db import models
from ckeditor_uploader.fields import RichTextUploadingField

class Post(models.Model):
    title = models.CharField(max_length=255)
    date = models.DateField(auto_now_add=True)
    demo = models.TextField()
    desc = RichTextUploadingField()
    img = models.ImageField(upload_to='news')

    def __str__(self):
        return self.title


class Project(models.Model):
    title = models.CharField(max_length=255)
    project = models.FileField(upload_to='projects')
    fullName = models.CharField(max_length=255)
    nationalCode = models.CharField(max_length=255)
    phoneNumber = models.CharField(max_length=255)
    field = models.CharField(max_length=255)
    sub_field = models.CharField(max_length=255)

    def __str__(self):
        return self.title

