from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('register/', views.register, name="register"),
    path('about/', views.about, name="about"),
    path('news/', views.news, name="news"),
    path('detail/<id>', views.detail, name="detail"),
    path('addProject/', views.addProject, name="addProject"),
]
