from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, JsonResponse
from .models import Post, Project


def index(request):
    news = Post.objects.all().order_by('-id')[:3]
    context = {
        'news': news
    }
    return render(request, 'index.html', context)


def register(request):
    return render(request, 'register2.html')

    
def about(request):
    return render(request, 'about.html')


def news(request):
    news = Post.objects.all().order_by('-id')
    return render(request, 'news.html', {'news': news})


def detail(request, id):
    news = get_object_or_404(Post, pk=id)
    return render(request, 'detail.html', {'news': news})

def addProject(request):
    if request.method == "POST":
        title = request.POST['title']
        projectFile = request.FILES['projectFile']
        fullName = request.POST['fullName']
        nationalCode = request.POST['nationalCode']
        phoneNumber = request.POST['phoneNumber']
        field = request.POST['field']
        sub_field = request.POST['sub_field']
        project = Project()
        project.title = title
        project.project = projectFile
        project.fullName = fullName
        project.nationalCode = nationalCode
        project.phoneNumber = phoneNumber
        project.field = field
        project.sub_field = sub_field
        project.save()
        return JsonResponse({'status': 'saved'})
    else:
        return JsonResponse({'status': 'invalidMethod'})